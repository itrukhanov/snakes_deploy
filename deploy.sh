#!/bin/sh
yum install -y python3-pip

pip3 install https://bitbucket.org/itrukhanov/snakes_server/get/master.zip
sudo PATH=/usr/local/bin:$PATH -i -u ec2-user nohup snakes_server &

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
nvm install --lts

yum -y install httpd
service httpd start

wget https://bitbucket.org/itrukhanov/snakes_web/get/master.tar.bz2
tar xvf master.tar.bz2
cd *snakes_web*
npm install
export SNAKES_HOST=`curl -s http://169.254.169.254/latest/meta-data/public-ipv4`
npx webpack --mode production
mount --bind ./dist/ /var/www/html/
